\chapter{Groupe modulaire}
\label{chap:groupe}

Dans tout ce mémoire, la lettre $\H$ désignera le demi-plan de Poincaré
$\{z\in\CC:\Im{z}>0\}$.


\section{Réseaux et groupe modulaire}

Commençons cette section par définir quelques notions de géométrie des nombres. On trouvera des compléments en
bibliographie (voir en particulier \cite{samuel,hindry}).

\begin{definition}
  Un sous-groupe discret de rang $n$ de $\R^n$ est appelé réseau de
  $\R^n$.
\end{definition}

Le cas qui nous intéresse ici est celui de $\CC$, qui s'identifie
naturellement à $\R^2$ comme $\R$-espace vectoriel.

\begin{definition}
  Un réseau de $\CC$ est un sous-groupe discret de $\CC$ de la forme
  $\Lambda=\Res{\omega_1}{\omega_2}$ où $(\omega_1,\omega_2)$ est une
  famille d'éléments de $\CC$ libre sur $\R$, la base du réseau. On
  ajoute de plus la convention de normalisation
  $\omega_1/\omega_2\in\H$.
\end{definition}

Deux nombres complexes $\omega_1$ et $\omega_2$ vérifiant cette
condition de normalisation définissent ainsi un réseau de
$\CC$. Cependant, une base d'un tel réseau n'est pas unique. Le lemme
suivant permet de caractériser ces bases.

\begin{lemme}\label{lemme:bases-reseau}
  Soient $\Lambda=\Res{\omega_1}{\omega_2}$ et
  $\Lambda'=\Res{\omega'_1}{\omega'_2}$ deux réseaux de $\CC$, où
  $\omega_1/\omega_2\in\H$ et $\omega'_1/\omega'_2\in\H$. Alors
  $\Lambda=\Lambda'$ si et seulement s'il existe $\abcd\in\SL$ telle
  que
  \[\begin{pmatrix}\omega'_1 \\
    \omega'_2\end{pmatrix} = \abcd\begin{pmatrix}\omega_1\\ \omega_2\end{pmatrix}.\]
\end{lemme}

Ce lemme amène donc à considérer un groupe de matrices particulier.

\begin{definition}
  On appelle groupe modulaire le groupe $\SL$. Il s'agit du groupe des
  matrices d'ordre $2$ à coefficients entiers de déterminant $1$. On peut
  encore écrire
  \[\SL=\left\{\abcd\in\mathscr{M}_2(\Z):ad-bc=1\right\}\]
\end{definition}

\begin{proof}[Démonstration du lemme \ref{lemme:bases-reseau}]
  L'égalité matricielle est claire à partir du moment où l'on écrit
  $\omega'_1$ et $\omega'_2$ dans la base de $\Lambda$. Il reste donc
  à montrer que la matrice $\abcd$ est élément du groupe
  modulaire.
  
  On a alors :
  \begin{align*}
    \Im{\frac{\omega'_1}{\omega'_2}}&=\Im{\frac{a\omega_1+b\omega_2}{c\omega_1+d\omega_2}}\\
    &=
      \Im{\frac{a\frac{\omega_1}{\omega_2}+b}{c\frac{\omega_1}{\omega_2}+d}}\\
                                    &=\frac{ad\Im{\frac{\omega_1}{\omega_2}}+bc\Im{\frac{\overline{\omega_1}}{\overline{\omega_2}}}}{\left|c\frac{\omega_1}{\omega_2}+d\right|^2}\\
    &=\frac{(ad-bc)\Im{\frac{\omega_1}{\omega_2}}}{\left|c\frac{\omega_1}{\omega_2}+d\right|^2}
  \end{align*}

  Pour vérifier la condition de normalisation, il est alors nécessaire que $ad-bc$ soit positif. Comme la
  matrice est inversible dans $\Z$, ce nombre vaut $1$ et $\abcd$ est alors dans $\SL$.
\end{proof}

\section{Tores complexes}

Un réseau est un sous-groupe discret du groupe additif $\CC$. Il est
donc distingué comme sous-groupe d'un groupe abélien, et nous pouvons légitimement nous intéresser au quotient.

\begin{definition}
  Un tore complexe est un quotient du plan complexe par un réseau
  \[\CC/\Lambda=\{z+\Lambda:z\in\CC\}.\]
  Il s'agit d'un groupe abélien pour l'addition provenant de $\CC$.
\end{definition}

Géométriquement, le compact $K=\{a\omega_1+b\omega_2:a,b\in[0,1]\}$
voit ses arêtes parallèles identifiées deux à deux, ce qui fournit
effectivement une surface de genre $1$ lorsque l'on quotiente $\CC$
par $\Res{\omega_1}{\omega_2}$.

\begin{proposition}
  Soit $\varphi:\CC/\Lambda\to\CC/\Lambda'$ une fonction holomorphe
  entre deux tores complexes. Il existe alors $m,b\in\CC$ vérifiant
  $m\Lambda\subset\Lambda'$ et tels que
  $\varphi(z+\Lambda)=mz+b+\Lambda'$. Cette fonction est inversible
  \ssi{} $m\Lambda=\Lambda'$.
\end{proposition}

\begin{corollaire}\label{holomorphisme-groupe}
  Soit $\varphi:\CC/\Lambda\to\CC/\Lambda'$ une fonction holomorphe
  entre deux tores complexes telle que
  $\varphi(z+\Lambda)=mz+b+\Lambda'$ et $m\Lambda\subset\Lambda'$. \LASSE :
  \begin{enumerate}
  \item $\varphi$ est un morphisme de groupe~;
  \item $b\in\Lambda'$, et par conséquent
    $\varphi(z+\Lambda)=mz+\Lambda'$~;
  \item $\varphi(0)=0$.
  \end{enumerate}
\end{corollaire}

Considérons le réseau $\Lambda=\Res{\omega_1}{\omega_2}$ où $\omega_1/\omega_2\in\H$. Notons
$\tau=\omega_1/\omega_2$ et $\Lambda_\tau$ le réseau $\Lambda_\tau=\Res{\tau}{}$.  Il est possible d'écrire
$\Lambda$ sous la forme $\omega_2\left(\Res{\frac{\omega_1}{\omega_2}}{}\right)=\omega_2\Lambda_\tau$,
c'est-à-dire que tout réseau est, à homothétie près, de la forme $\Res{\tau}{}$ où $\tau\in\H$.

Puisque $\Lambda=\omega_2\Lambda_\tau$, le corollaire
\ref{holomorphisme-groupe} montre que l'application
$\varphi_\tau:\CC/\Lambda_\tau\to\CC/\Lambda$ définie par
$\varphi(z+\Lambda_\tau)=\omega_2z+\Lambda$ est un isomorphisme de
groupes holomorphe. Tout tore complexe est donc isomorphe à un tore
engendré par un élément $\tau\in\H$ et par $1$.\\
Ce $\tau$ n'est pas unique et l'ensemble de ces éléments est obtenu
par une action du groupe modulaire :

Le lemme \ref{lemme:bases-reseau} conduit donc à s'intéresser à l'action du groupe
modulaire sur le demi-plan de Poincaré suivante :

\[\forall\abcd\in\SL,\forall\tau\in\H, \abcd(\tau)=\frac{a\tau+b}{c\tau+d}\]

% \begin{definition}
%   On appelle isogénie un morphisme de groupes holomorphes entre deux tores complexes.
% \end{definition}

% \begin{proposition}
%   Une isogénie est surjective et a un noyau fini.
% \end{proposition}

% \begin{proof}
%   Montrons la seconde partie.\\
%   Soit $\varphi:\CC/\Lambda\to\CC/\Lambda'$ une isogénie.
%   $\CC/\Lambda$ est compact comme image de n'importe quel compact de
%   $\CC$ contenant une maille du réseau $\Lambda$ (si
%   $\Lambda=\Res{\omega_1}{\omega_2}$, on peut par exemple prendre
%   $\{a\omega_1+b\omega_2:a,b\in[0,1]\}$) par la projection canonique
%   $\CC\to\CC/\Lambda$. De plus, $\ker\varphi$ est discret et un
%   ensemble discret inclus dans un compact est fini.
% \end{proof}

% On étudie maintenant quelques isogénies particulières.

% \begin{definition}
%   Soit $\Lambda$ un réseau de $\CC$. Pour tout entier $N$ non nul, on
%   définit l'application de multiplication par $N$ comme
%   \begin{align*}
%     [N]:\CC/\Lambda &\to \CC/\Lambda \\
%     z+\Lambda &\mapsto Nz+\Lambda
%   \end{align*}
%   Son noyau est le sous-groupe des points de $N$-torsion de
%   $\CC/\Lambda$, isomorphe à $E[N]:=\Z/N\Z\times\Z/N\Z$ où la lettre
%   $E$ désigne le tore $\CC/\Lambda$.
% \end{definition}

% $[N]$ est une isogénie car $N\Lambda\subset\Lambda$.

% à terminer si c'est pertinent

\section{Sous-groupes de congruence}

Dans cette section, nous poursuivons l'étude du groupe modulaire et de son action sur le demi-plan de Poincaré
$\H$ en étudiant certains de ses sous-groupes et des applications qui sont \og stables\fg{} (en un sens à
préciser) par leur action. Ces notions émergent naturellement lors de l'étude des entiers qui sont sommes de
quatre carrés. Nous nous permettrons donc un bref détour par la géométrie des nombres.

\begin{theoreme}
  Tout nombre entier naturel est somme de quatre carrés.
\end{theoreme}

C'est une conséquence du théorème de Minkowski, dès que l'on considère le bon réseau de $\R^4$. La question
naturelle à se poser ensuite consiste à connaître le nombre de façons d'écrire un entier naturel comme somme de
quatre carrés.

\begin{definition}
  Plus généralement, pour deux entiers $n,k\in\N$, on introduit le
  nombre de représentations de $n$ comme somme de $k$ carrés,
  \[r(n,k)=|\{v\in\Z^k:n=v_1^2+\dotsb+v_k^2\}|.\]
\end{definition}

On a alors le résultat suivant.

\begin{proposition}
  Soient $n,k\in\N^*$. Si $i,j$ sont deux entiers naturels vérifiant $i+j=k$, alors 
  \[r(n,k)=\sum_{l+m=n}r(l,i)r(m,j)\]
  où les indices $l,m$ sont pris dans $\N$.
\end{proposition}

La démonstration se fait par dénombrement et est laissée
au lecteur. Ce résultat rappelle le produit de Cauchy de deux séries
entières, et nous pousse donc à introduire la fonction génératrice de $r$.

\begin{definition}
  Soit $k$ un entier naturel. La \textit{fonction génératrice} du
  nombre de représentations comme somme de $k$ carrés est donnée par
  \[\theta(\tau,k)=\sum_{n=0}^\infty r(n,k)q^n, \quad
  q=e^{2i\pi\tau},\tau\in\H.\]
\end{definition}

On peut remarquer que la fonction $\theta$ est $1$-périodique pour sa
première variable et vérifie la relation
\[\forall\tau\in\H, \quad \theta(\tau,k_1)\theta(\tau,k_2)=\theta(\tau,k_1+k_2)\]

Cette dernière relation permet d'écrire $\theta(\tau,n)$ comme $\theta(\tau,1)^n$. Nous posons donc
\[\theta:\tau\in\H\mapsto\theta(\tau,1).\]

Par définition
\[\theta(\tau)=\sum_{d\in\Z}e^{2i\pi d^2\tau}.\]

La formule sommatoire de Poisson, couplée au théorème d'unicité du prolongement analytique, fournit la
transformation\footnote{Le symbole $\sqrt{  }$ désigne dans ce cas la racine carrée principale (c'est-à-dire la
  puissance $1/2$ que l'on définit par le logarithme principal), ce qui est ici licite car $-2i\tau$ a une
  partie réelle positive.}
\[\theta\left(-\frac1{4\tau}\right)=\sqrt{-2i\tau}\theta(\tau).\]

La matrice $
\begin{pmatrix*}[r]
  0 & -1 \\ 4 & 0
\end{pmatrix*}
$ permettant cette transformation n'a pas pour déterminant $1$ mais c'est le cas de la matrice $
\begin{pmatrix}
  1 & 0 \\ 4 & 1
\end{pmatrix}=
\begin{pmatrix*}
  0 & 1/4 \\ -1 & 0
\end{pmatrix*}
\begin{pmatrix*}[r]
 1 & -1 \\ 0 & 1 
\end{pmatrix*}
\begin{pmatrix*}
  0 & -1 \\ 4 & 0
\end{pmatrix*}
$ qui envoie $\tau$ sur $\tau/(4\tau+1)$. 

Appliquer les transformations associées\footnote{Là encore, prendre la racine carrée principale est licite.} donne
\[\theta\left(\frac{\tau}{4\tau+1}\right)=\sqrt{4\tau+1}\theta(\tau).\]

Revenant à notre problème initial, qui consistait à considérer $\theta(\tau,4)=\theta(\tau)^4$, nous trouvons
une seconde formule de transformation (en plus de celle qui donne la $1$-périodicité)
\[\theta\left(\frac{\tau}{4\tau+1},4\right)=(4\tau+1)^2\theta(\tau,4).\]

Cet exemple motive donc l'introduction de fonctions invariantes par l'action de certaines matrices de
$\SL$. Les définitions qui suivent sur les sous-groupes de congruence et les formes modulaires vont clarifier cela.

\begin{definition}
  Soit $N$ un entier naturel non nul. Le sous-groupe de congruence
  principal de niveau $N$, noté $\Gamma(N)$, est le noyau de
  l'application 
  \[\SL\to\mathrm{SL}_2(\Z/N\Z),\]
  c'est-à-dire l'ensemble
  \[\Gamma(N)=\left\{\abcd\in\SL:\abcd\equiv\begin{pmatrix}1&0\\0&1\end{pmatrix}\;(\mathrm{mod.}\;
    N)\right\}.\] où les congruences sont vues coefficient par coefficient.
  Par définition, c'est évidemment un sous-groupe distingué de $\SL$.
\end{definition}


Nous pouvons maintenant définir la notion de sous-groupe de
congruence, qui nous intéressera dans toute la suite.

\begin{definition}
  Un sous-groupe $\Gamma$ de $\SL$ est un sous-groupe de congruence
  s'il existe $N\in\N^*$ tel que $\Gamma(N)\subset\Gamma$. On dit
  alors que $\Gamma$ est un sous-groupe de congruence de niveau\footnote{Un sous-groupe de congruence peut
    évidemment avoir plusieurs niveaux, comme c'est le cas pour $\SL$, bien que ce dernier soit le plus
    souvent vu comme étant de niveau 1.} $N$.
\end{definition}

Deux familles de sous-groupes de congruence sont particulièrement importantes.

\begin{definition}
  Soit $N\in\N^*$, on définit les sous-groupes de congruence
  \[\Gamma_0(N)=\left\{\abcd\in\SL:\abcd\equiv\begin{pmatrix}*&*\\0&*\end{pmatrix}\;(\mathrm{mod.}\;N)\right\}\]
  et
  \[\Gamma_1(N)=\left\{\abcd\in\SL:\abcd\equiv\begin{pmatrix}1&*\\0&1\end{pmatrix}\;(\mathrm{mod.}\;N)\right\}.\]
\end{definition}


Avant d'aller plus loin, nous allons calculer les cardinaux ou indices de ces sous-groupes.

Si $p$ est premier, $\abs{\mathrm{GL}_2(\Z)}=(N^2-1)(N^2-N)$ (il suffit de compter le nombre de bases, soit
tous les vecteurs non nuls pour le choix de la première colonne, et tous les vecteurs non colinéaires à la
première colonne pour le choix de la seconde). $\SL$ étant le noyau de l'application
$\det:\mathrm{GL}_2(\Z)\to(\Z/N\Z)^\times$, son cardinal est
\[\abs{\mathrm{SL}_2(\Z/p\Z)}=p^3\left(1-\frac1{p^2}\right).\]
Pour tout $e\geq 1$, le noyau de l'application $\mathrm{SL}_2(\Z/p^{e+1}\Z)\to\mathrm{SL}_2(\Z/p^e\Z)$ est un
ensemble dont les matrices sont déterminées seulement par trois coefficients (il y a $p$ possibilités pour les
coefficients non diagonaux, et $p$ possibilités également pour le premier coefficient, le dernier étant son
inverse). On obtient par théorème de factorisation
\[\abs{\mathrm{SL}_2(\Z/p^{e+1}\Z)}=p^3\abs{\mathrm{SL}_2(\Z/p^e\Z)}=p^3(e+1)\left(1-\frac1{p^2}\right)\]
et le théorème chinois permet alors de calculer
\[\abs{\mathrm{SL}_2(\Z/N\Z)}=N^3\prod_{\substack{p\in\P\\ p\mid N}}\left(1-\frac1{p^2}\right).\]

L'indice de $\Gamma(N)$ dans $\SL$ est donc
\[[\SL:\Gamma(N)]=N^3\prod_{p\mid N}\left(1-\frac1{p^2}\right).\]

L'application
\[\function{\Gamma_1(N)}{\Z/N\Z}{\abcd}{b \mod{N}}\]
est surjective de noyau $\Gamma(N)$, donc $[\Gamma_1(N):\Gamma(N)]=N$.
De même l'application
\[\function{\Gamma_1(N)}{(\Z/N\Z)^\times}{\abcd}{d \mod{N}}\]
est surjective et son noyau est $\Gamma_1(N)$, ce qui fait de $\Gamma_1(N)$ un sous-groupe distingué dans
$\Gamma_0(N)$, d'indice $[\Gamma_0(N):\Gamma_1(N)]=\phi(N)$.
Il s'ensuit que
\[[\SL:\Gamma_0(N)]=N\prod_{p\mid N}\left(1+\frac1{p}\right).\]

\section{Formes modulaires}

\begin{definition}
  Soit $\gamma=\abcd\in\SL$. On définit le \textit{facteur d'automorphie} $j(\gamma,\tau)\in\CC$ pour $\tau\in\H$ par
  \[j(\gamma,\tau)=c\tau+d.\]
\end{definition}

\begin{definition}
  Soient $\gamma=\abcd\in\SL$ et $k\in\Z$. On définit l'\textit{opérateur de
    poids $k$} $[\gamma]_k$ sur les fonctions $f:\H\to\CC$ par
  \[\forall\tau\in\H, (f[\gamma]_k)(\tau)=j(\gamma,\tau)^{-k}f(\gamma(\tau)).\]
\end{definition}

On peut alors définir les propriétés élémentaires de ces deux notions.

\begin{lemme}
  Pour tous $\gamma,\gamma'\in\SL$, pour $\tau\in\H$,
  \begin{enumerate}
  \item $j(\gamma\gamma',\tau)=j(\gamma,\gamma'(\tau))j(\gamma',\tau)$,
  \item $(\gamma\gamma')(\tau)=\gamma(\gamma'(\tau))$,
  \item $[\gamma\gamma']_k=[\gamma]_k[\gamma']_k$,
  \item $\displaystyle\Im{\gamma(\tau)}=\frac{\Im{\tau}}{|j(\gamma,\tau)|^2}$,
  \item $\displaystyle\frac{\dd{\gamma}(\tau)}{\dd\tau}=\frac{1}{j(\gamma,\tau)^2}$.
  \end{enumerate}
\end{lemme}

\begin{definition}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$.  Une fonction
  $f:\H\to\CC$ est dite \textit{quasi modulaire de poids $k$ par
    rapport à $\Gamma$} si $f$ est méromorphe sur $\H$ et si, pour
  tout $\gamma\in\Gamma$,
  \[f[\gamma]_k=f.\]
\end{definition}

Le lemme précédent montre que la quasi modularité par rapport à un sous-groupe de congruence $\Gamma$ peut
n'être vérifiée que pour des générateurs de $\Gamma$, si l'on en connaît.

Si $f$ est quasi modulaire par rapport à $\Gamma$, $f$ est nécessairement $h$-périodique pour un certain
$h\geq 1$ minimal car le groupe $\Gamma$ contient la matrice $
\begin{pmatrix}
  1 & h \\ 0 & 1
\end{pmatrix}$. En posant $\D=\{q\in\CC:\abs{q}<1\}$ le disque unité complexe et $\D'=\D\setminus\{0\}$ le
disque unité épointé, nous savons que l'application $\tau\mapsto e^{2i\pi\tau/h}$ envoie $\H$ sur
$\D'$. L'application
\[\function[g]{\D'}{\CC}{q_h}{f\left(\frac{\log(q_h)}{2i\pi}\right)}\]
est donc bien définie, bien que le logarithme ne soit déterminé que modulo $2i\pi$, et
$f(\tau)=g(e^{2i\pi\tau/h})$. Si $f$ est holomorphe sur $\H$, l'application $g$ l'est alors aussi car le
logarithme peut être défini analytiquement autour de chaque point, et elle possède un développement en série
de Laurent
\[g(q_h)=\sum_{n\in\Z}a_nq_h^n, \quad q_h\in\D'.\]

Or $\abs{q_h}=e^{-2\pi\Im{\tau}}\to 0$ quand $\Im{\tau}\to\infty$, et en s'imaginant $\infty$ comme étant
$i\infty$, on dira que $f$ est holomorphe à l'infini si $g$ se prolonge analytiquement au point $q_h=0$, ce qui
signifie que le développement de Laurent est en fait un développement en série de Fourier de $f$. 

Le travail que nous venons d'effectuer va motiver la définition de forme modulaire. Pour garder l'espace
qu'elles forment de dimension finie, il sera nécessaire de requérir non seulement qu'elles soient holomorphes
sur $\H$, mais aussi qu'elles le soient en des points limites. Dès que $\Gamma$ est différent de $\SL$, il
n'est plus suffisant de demander l'holomorphie à l'infini, et l'on va donc également adjoindre les nombres
rationnels.

\begin{definition}[pointe]
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$. On appelle pointe de $\Gamma$ toute classe
  d'équivalence de $\Q\cup\{\infty\}$ sous $\Gamma$.
\end{definition}

Le groupe $\SL$ n'a qu'une pointe et par conséquent, un sous-groupe de congruence $\Gamma$ a au plus
$[\SL:\Gamma]$ pointes.

On peut donc enfin donner la définition suivante.

\begin{definition}\label{def:forme-modulaire}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$ et soit $k$ un
  entier. Une fonction $f:\H\to\CC$ est une \textit{forme modulaire}
  de poids $k$ par rapport à $\Gamma$ si
  \begin{enumerate}
  \item $f$ est holomorphe sur $\H$,
  \item $f$ est quasi modulaire de poids $k$ par rapport à
    $\Gamma$,
  \item\label{condition-mod-3} $f[\alpha]_k$ est holomorphe à l'infini pour tout $\alpha\in\SL$.
  \end{enumerate}
  L'ensemble des formes modulaires de poids $k$ par rapport à $\Gamma$ est noté $\M_k(\Gamma)$.
\end{definition}

\begin{proposition}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$ et niveau $N$. Posons, pour $\tau\in\H$,
  $q_N=e^{2i\pi\tau/N}$. Si $f:\H\to\CC$ satisfait aux deux premières conditions de la définition précédente,
  et que ses coefficients de Fourier d'indice non nul sont des $O(n^r)$ pour un certain $r\geq 1$, alors elle
  vérifie aussi la condition \ref{condition-mod-3}, et réciproquement.
\end{proposition}

Le produit de deux formes modulaires par rapport au même groupe $\Gamma$ est encore une forme modulaire, dont
le poids est la somme des poids de ses facteurs. La somme directe
\[\M(\Gamma)=\bigoplus_{k\in\Z}\M_k(\Gamma)\]
est donc un anneau gradué.

\section{Quelques exemples}

Nous nous sommes déjà aventurés assez loin dans les définitions et il conviendrait de s'assurer que nous ne
somme pas en train de faire de la théorie de l'ensemble vide en étudiant quelques exemples de formes
modulaires. Pour plus de simplicité, nous nous placerons dans $\SL$, qui est le sous-groupe de congruence le
plus évident. Des exemples pour d'autres sous-groupes de congruence seront étudiés dans le chapitre
\ref{chap:eisenstein}.

Un premier exemple particulièrement évident de formes modulaires est fourni par la famille des fonctions
constantes sur $\H$. Celles-ci sont de poids nul, à l'exception de la fonction nulle qui est modulaire pour
n'importe quel poids. Nous allons maintenant nous concentrer sur des exemples non triviaux.

Les séries d'Eisenstein, que nous nous apprêtons à définir, sont des analogues bidimensionnels de la fonction
$\zeta$ de Riemann\footnote{La variable $s$ de $\zeta$, qui est ici un
entier $k$, devient un paramètre de la série, et la variable que l'on
manipule dans le cas des séries d'Einsenstein définit un réseau.}. On peut les définir pour tout entier $k>2$ pair sur les points d'un réseau
$\Lambda_\tau=\tau\Z\oplus\Z.$
\[G_k(\tau)=\psum_{\omega\in\Lambda_\tau}\frac1{\omega^k}=\psum_{(c,d)\in\Z^2}\frac1{(c\tau+d)^k}\]
où le symbole prime signifie que l'on ne somme pas sur l'indice $(0,0)$. On peut en fait définir de telles
séries sur n'importe quel réseau $\Lambda$ de la même façon.

Notons $L'=\Z^2\setminus\{(0,0)\}$ et, pour tout $n\geq 1$, $K_n=\{(c,d)\in L':\sup(c,d)=n\}$.
Il est clair que $L'=\bigcup_{n>0}K_n$.
\[\sum_{(c,d)\in K_n}\frac1{\norminf{(c,d)}^k}=\frac{4(2n+1)-4}{n^k}=\frac8{n^{k-1}},\]
terme général d'une série convergente par le critère de Riemann car $k-1>1$.

La série $\sum_{(c,d)\in L'}\frac1{\norminf{(c,d)}^k}$ est donc convergente. Définissons maintenant la norme
\[\function[N]{\R^2}{\R_+}{(c,d)}{\abs{c\tau+d}}\]
sur $\R^2$. Cet espace étant de dimension finie, $N$ est équivalente à $\norminf{\cdot}$ et il existe donc
$\beta>0$ tel que $\norminf{\cdot}<\beta N$.

On en déduit alors la convergence absolue de la série d'Eisenstein de poids $k$ par critère de
majoration. Soit $K$ un compact de $\H$. Notons $A=\min\Im{\tau}>0$ et $B=\max\abs{\Re{\tau}}>0$ pour
$\tau\in K$. Alors $\abs{c\tau+d}\geq \Re{c\tau+d}\geq cB_c+d$ (où $B_c=B$ si $c\leq 0$ et $B_c=-B$ sinon) et
puisque la série de terme général $\frac1{(cB_c+d)^k}$ converge absolument, la série d'Eisenstein converge
normalement donc uniformément sur tout compact. $G_k$ est donc holomorphe sur $\H$ car le terme général est
holomorphe sur $\H$.

Montrons maintenant la quasi modularité
\[G_k(\gamma(\tau))=\psum_{(c',d')}\frac1{\left(c'\frac{a\tau+b}{c\tau+d}+d'\right)^k}=(c\tau+d)^k\psum_{(c',d')}\frac1{((c'a+d'c)\tau+(c'b+d'd))^k}.\]
Or $(c'a+d'c,c'b+d'd)=(c',d')\abcd$ et la somme de droite vaut donc exactement $G_k(\tau)$, d'où
\[G_k(\gamma(\tau))=(c\tau+d)^kG_k(\tau).\]
La série $G_k$ est donc quasi modulaire de poids $k$

D'après les calculs précédents, $G_k$ est bornée sur tout ensemble du type
$\{\tau\in\H:\Im{\tau}\geq A,\abs{\Re{\tau}}\leq B\}$ et
puisque $G_k$ est quasi modulaire pour $\SL$, elle est $1$-périodique donc est bornée quand
$\Im{\tau}\to\infty$. C'est donc une forme modulaire de poids $k$ pour $\SL$.

Nous souhaiterions maintenant calculer sa série de Fourier. Commençons par un lemme.

\begin{lemme}
  Soit $\tau\in\H$, on a les identités
  \begin{equation}\label{eq:formuledelamort}
    \frac1\tau+\sum_{d=1}^\infty\left(\frac1{\tau-d}+\frac1{\tau+d}\right)=\pi\cot\pi\tau=i\pi-2i\pi\sum_{m=0}^\infty
    q^m, \quad q=e^{2i\pi\tau}
\end{equation}
\end{lemme}

\begin{proof}
  On peut tout d'abord écrire
  \[\sin(\pi\tau)=\pi\tau\prod_{n=1}^\infty\left(1-\frac{\tau^2}{n^2}\right).\]
  Le produit converge d'après le théorème 15.6 p.~150 de \cite{rudin} car $\tau\mapsto\tau^2/n^2$ est le
  terme général (non identiquement nulle sur chaque composante connexe) d'une série uniformément convergente
  sur tout compact, et l'égalité provient de l'unicité du prolongement analytique.  En prenant la dérivée
  logarithmique de cette expression de chaque côté, on obtient la première égalité.  Pour la seconde égalité,
  on développe $\sin(\pi\tau)$ et $\cos(\pi\tau)$ à l'aide de l'exponentielle puis on regroupe les termes en
  reconnaissant au passage une somme géométrique
  \[\pi\cot\pi\tau=-i\pi\left(\frac{e^{-i\pi\tau}+e^{i\pi\tau}}{e^{-i\pi\tau}-e^{i\pi\tau}}\right)=i\pi(1+q)\sum_{m=0}^\infty
  q^m.\]
  Un développement et un changement d'indice donnent le résultat.
\end{proof}

Dérivons maintenant l'égalité (\ref{eq:formuledelamort}) $k-1$ fois par rapport à $\tau$.
\begin{equation}
  \label{eq:jolie-formule}
  \sum_{d\in\Z}\frac1{(\tau+d)^k}=\frac{(-2i\pi)^k}{(k-1)!}\sum_{m=1}^\infty m^{k-1}q^m, k\geq 2
\end{equation}

Or, pour $k$ pair et strictement supérieur à 2,
\[\psum_{(c,d)}\frac1{(c\tau+d)^k}=\sum_{d\neq 0}\frac1{d^k}+\sum_{c\neq
  0}\left(\sum_{d\in\Z}\frac1{(c\tau+d)^k}\right)=\sum_{d\neq
  0}\frac1{d^k}+2\sum_{c=1}^\infty\left(\sum_{d\in\Z}\frac1{(c\tau+d)^k}\right),\]
et donc
\[\psum_{(c,d)}\frac1{(c\tau+d)^k} = 2\zeta(k)+2\frac{(2i\pi)^k}{(k-1)!}\sum_{c=1}^\infty\sum_{m=1}^\infty
  m^{k-1}q^{cm}.\] Définissons la fonction arithmétique
\[\sigma_{k-1}(n)=\sum_{\substack{m\mid n\\m>0}}m^{k-1}\]
qui permet ensuite de simplifier en
\[G_k(\tau)=2\zeta(k)+2\frac{(2i\pi)^k}{(k-1)!}\sum_{n=1}^\infty\sigma_{k-1}(n)q^n.\]
Cette fonction peut enfin être normalisée en divisant par son terme constant $2\zeta(k)$. La forme normalisée
est notée $E_k$.

\begin{definition}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$.
  On appelle \textit{forme cuspidale} (ou \textit{parabolique}) de poids $k$ par rapport à $\Gamma$ toute
  forme modulaire de poids $k$ par rapport à $\Gamma$ dont la décomposition de Fourier n'a pas de terme
  constant
  \[f(\tau)=\sum_{n=1}^\infty a_nq^n, \quad q=e^{2i\pi\tau}.\]
  L'ensemble des formes cuspidales par rapport à $\Gamma$ est noté $\S_k(\Gamma)$.
\end{definition}

Comme pour les formes modulaires, l'espace
\[\S(\Gamma)=\bigoplus_{k\in\Z}\S_k(\Gamma)\]
est un anneau gradué, et c'est même un idéal de $\M_k(\Gamma)$.

Il est assez clair que les séries d'Eisenstein ne sont pas des formes cuspidales. On peut néanmoins en
construire à partir de ces séries. Donnons deux exemples très classiques.

Posons $g_2(\tau)=60G_4(\tau)$ et $g_3(\tau)=140G_6(\tau)$. La fonction
\[\function[\Delta]{\H}{\CC}{\tau}{g_2(\tau)^3-27g_3(\tau)^2}\]
que l'on appelle \textit{discriminant} est alors modulaire de poids $12$ d'après les propriétés précédentes et
les coefficients choisis montrent que son terme constant est nul.

On peut montrer que le terme en $q$ a pour coefficient de Fourier
$a_1=(2\pi)^{12}$, donc $\Delta\neq 0$. Le discriminant $\Delta$ ne
s'annule en fait en aucun point de $\H$.

Il s'ensuit que la \textit{fonction modulaire}
\[\function[j]{\H}{\CC}{\tau}{1728\frac{(g_2(\tau))^3}{\Delta(\tau)}}\]
est holomorphe sur $\H$, et invariante sous l'action de $\SL$, donc est modulaire de poids $0$. On l'appelle
\textit{invariant modulaire}.






%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../mémoire"
%%% End:
