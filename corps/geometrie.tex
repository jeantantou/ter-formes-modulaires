\chapter{Étude géométrique des courbes modulaires}
\label{chap:geometrie}

Dans ce chapitre, on étudie les courbes modulaires, c'est-à-dire
l'espace des orbites de $\H$ sous l'action d'un sous-groupe de
congruence. Il se trouve que ces courbes ont de bonnes propriétés
topologiques et sont des surfaces de Riemann.


Comme on l'a vu au chapitre \ref{chap:groupe}, le groupe modulaire $\SL$
agit sur le demi-plan de Poincaré $\H$ par
\[\gamma\cdot\tau=\gamma(\tau)\]

Cette action peut se restreindre sans problème à tout sous-groupe de
congruence $\Gamma$ de $\SL$. On peut alors s'intéresser à l'espace des
orbites pour cette action. Cet espace, appelé courbe modulaire, est défini par
\[Y(\Gamma)=\Gamma\setminus\H=\{\Gamma\tau:\tau\in\H\}.\]

\section{Topologie}

Commençons par étudier la topologie de $Y(\Gamma)$. Cette courbe étant définie comme un quotient, il est
naturel de la munir de la topologie quotient provenant de $\H$ par la projection canonique
$\pi:\tau\mapsto\Gamma\tau$. Le fait que le quotient provienne d'une action de groupe fait de $\pi$ une
application ouverte. On va montrer que cette topologie est
séparée\footnote{La proposition \ref{prelude-coro} dit en fait que
  $\SL$ agit proprement sur $\H$, et c'est une condition suffisante
  pour que la topologie soit séparée. On peut consulter
  \cite{bourbaki}, chap. III, p.~29. Nous le remontrons tout de même
  dans notre cas.} (au sens de Hausdorff).

\begin{proposition}\label{prelude-coro}
  Soient $\tau_1,\tau_2\in\H$. Alors il existe deux voisinages $U_1$
  de $\tau_1$ et $U_2$ de $\tau_2$ dans $\H$ vérifiant
  \[\forall\gamma\in\SL, (\gamma(U_1)\cap U_2\neq\vide \implies \gamma(\tau_1)=\tau_2).\]
\end{proposition}

\begin{corollaire}
  Quel que soit le sous-groupe de congruence $\Gamma$ de $\SL$, la
  topologie sur la courbe modulaire $Y(\Gamma)$ est séparée.
\end{corollaire}

\begin{proof}
  Soient $\pi(\tau_1)$ et $\pi(\tau_2)$ deux points distincts de
  $Y(\Gamma)$. Soient $U_1$ et $U_2$ des voisinages de $\tau_1$ et
  $\tau_2$ vérifiant les propriétés de la proposition
  \ref{prelude-coro}. Puisque $\gamma(\tau_1)\neq \gamma(\tau_2)$ pour
  tout $\gamma\in\Gamma$ (sinon ils fourniraient par passage au
  quotient le même point sur la courbe), la proposition
  \ref{prelude-coro} donne $\Gamma(U_1)\cap U_2 = \vide$ dans $\H$. Or
  $U_2\subset \Gamma(U_2)$ car $\Gamma$ contient l'identité, et donc
  $\Gamma(U_1)\cap U_2\subset \Gamma(U_1)\cap \Gamma(U_2) =
  \pi(U_1)\cap\pi(U_2)$ dans $Y(\Gamma)$. Donc $\pi(U_1)$ et $\pi(U_2)$
  sont des ensembles disjoints contenant respectivement $\pi(\tau_1)$
  et $\pi(\tau_2)$ dans $Y(\Gamma)$. Ce sont des voisinages car $\pi$
  est une application ouverte.
\end{proof}


\section{Cartes locales}

Notre objectif est de faire de la courbe $Y(\Gamma)$ une surface de
Riemann. Il faut donc la munir de coordonnées locales, à savoir
associer à chaque point $\pi(\tau)$ de $Y(\Gamma)$ un voisinage ouvert
$\tilde{U}$ localement homéomorphe à un ouvert de $\CC$ tel que les
applications de changement de cartes soient biholomorphes.

\begin{proposition}
  Soit $\tau\in\H$ un point fixé uniquement par la transformation
  identique de $\Gamma$ (l'identité ou son opposé). Il existe alors un
  voisinage ouvert $U$ de $\tau$ homéomorphe à son image $\pi(U)$
  par la restriction $\pi_{|U}:U\to\pi(U)$. L'inverse de cet
  homéomorphisme est alors une application coordonnée.
\end{proposition}

\begin{proof}
  Soit $\tau\in\H$ fixé uniquement par $I$ et $-I$ (si $-I\in\Gamma$). Alors pour tout
  $\gamma\in\Gamma\setminus\{I,-I\}$, $\gamma(\tau)\neq\tau$, et la contraposée de la proposition
  \ref{prelude-coro} donne l'existence d'un ouvert $U$ de $\tau$ vérifiant $\gamma(U)\cap U=\vide$. Cet ouvert
  ne contient donc qu'un seul représentant pour chaque classe et la restriction de $\pi$ à $U$ est injective.
  C'est donc un homéomorphisme de $U$ dans $\pi(U)$.
\end{proof}

Le cas des points fixés par plus d'éléments du groupe est plus
technique et nécessite une définition.

\begin{definition}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$. Un point $\tau\in\H$
  est un \textit{point elliptique} pour $\Gamma$ si l'inclusion $\{\pm
  I\}\subset \{\pm I\}\Gamma_\tau$ est stricte où $\Gamma_\tau$ est le
  stabilisateur de $\tau$ pour l'action de $\Gamma$. On appelle également
  \textit{elliptique} le point correspondant $\pi(\tau)$ de $Y(\Gamma)$.
\end{definition}

\section{Points elliptiques}

Le but de cette section est de prouver la proposition suivante.

\begin{proposition}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$. Pour tout point
  elliptique $\tau$ de $\Gamma$, le sous-groupe d'isotropie
  $\Gamma_\tau$ est cyclique.
\end{proposition}

On notera $Y(1)=\SL\setminus\H$ et $\D=\{\tau\in\H:\abs{\Re{\tau}}\leq
1/2,\abs{\tau}\geq 1\}.$

\begin{lemme}
  L'application $\pi:\D\to Y(1)$ est surjective.
\end{lemme}

\begin{lemme}
  Soient $\tau_1$ et $\tau_2$ deux points distincts de $\D$
  appartenant à la même orbite sous l'action de $\SL$. Alors de deux
  choses l'une :
  \begin{enumerate}
  \item $\Re{\tau_1}=\pm 1/2$ et $\tau_2=\tau_1\mp 1$ ou,
  \item $\abs{\tau_1}=1$ et $\tau_2=-1/\tau_1$.
  \end{enumerate}
\end{lemme}

\begin{proof}
  On peut supposer, quitte à échanger $\tau_1$ et $\tau_2$, que
  $\Im{\tau_2}\geq\Im{\tau_1}$. Les éléments $\tau_1$ et $\tau_2$ étant dans la
  même orbite, il existe $\gamma=\abcd\in\SL$ tel que
  $\tau_2=\gamma(\tau_1)$.

  On rappelle que $\Im{\gamma(\tau_1)}=\frac{\Im{\tau_1}}{\abs{c\tau_1+d}^2}$.  $\tau_1\in\D$ donc
  $\Im{\tau_1}\geq \sqrt{3}/2$ et puisque $\Im{\tau_2}\geq\Im{\tau_1}$ et que $\Im{\tau_2},\Im{\tau_1}>0$,
  alors $\abs{c\tau_1+d}^2\leq 1$.

  On a donc
  \[\abs{c}\frac{\sqrt{3}}{2}\leq
  \abs{c}\Im{\tau_1}\leq\abs{\Im{c\tau_1+d}}\leq\abs{c\tau_1+d}\leq
  1,\]
  d'où $\abs{c}\leq \sqrt{3}/2$ et, $c$ étant entier, on en déduit
  $\abs{c}\in\{0,1\}$.

  Il ne reste plus qu'à distinguer des différents cas. Si $c=0$, alors
  nécessairement $\gamma=\pm
  \begin{pmatrix}
    1 & b \\ 0 & 1
  \end{pmatrix}
  $ et $\Re{\tau_2}=\Re{\tau_1}\pm b$, ce qui force $\abs{b}=1$ et
  l'on retrouve bien le premier cas.

  Si $\abs{c}=1$, alors la condition $\abs{c\tau_1+d}^2\leq 1$ se
  simplifie en $\abs{\tau_1\pm d}^2\leq 1$, ce qui s'écrit encore
  \[\left(\Re{\tau_1}\pm d\right)^2+\left(\Im{\tau_1}\right)^2 \leq
  1,\]
  ce qui implique, sachant que $\Im{\tau_1}\geq \sqrt{3}/2$, que
  $\abs{\Re{\tau_1}\pm d}\leq 1/2$ et donc l'inégalité triangulaire à
  l'envers force $\abs{d}\in\{0,1\}$. Il y a donc de nouveau deux
  cas. Soit $\abs{d}=1$ et les inégalités précédentes doivent être des
  égalités, d'où $\Im{\tau_1}=\frac{\sqrt{3}}{2}$ et $\abs{\Re{\tau_1}\pm
    1}=\frac{1}{2}$, ce qui donne $\Re{\tau_1}=\pm 1/2$, on se retrouve
  alors à la fois dans le premier et le second cas.
  Si $d=0$, alors la condition $\abs{\tau_1\pm d}^2\leq 1$ se simplifie
  encore davantage en $\abs{\tau_1}\leq 1$ et comme $\tau_1\in\D$, cette
  inégalité est en fait une égalité. De plus, les parties imaginaires de
  $\tau_1$ et $\tau_2$ sont égales. Étant distincs mais de mêmes module et
  partie imaginaire, leurs parties réelles doivent être opposées, d'où
  la seconde condition.
\end{proof}

Ces deux lemmes montrent que $\D$ est un domaine fondamental pour
l'action de $\SL$ sur $\H$.

On va maintenant montrer que si $\tau\in\H$ est un point elliptique
pour $\SL$, alors son stabilisateur est fini et il n'y a que peu de
choix pour les éléments qui stabilisent $\tau$, et donc peu de choix
pour $\tau$ aussi.

Soient donc $\tau$ un point elliptique et $\gamma=\abcd\in\SL$ fixant
$\tau$. On a donc $a\tau+b=\tau(c\tau+d)$, ce qui en développant
montre que $\tau$ est racine du polynôme $cX^2+(d-a)X-b$. Comme $\tau$
est dans $\H$, il n'est pas dans $\R$ et ce polynôme n'est donc pas du
premier degré (sa seule racine serait alors réelle, et même
rationnelle). Donc $c\neq 0$. On calcul alors le discriminant de ce
polynôme, et l'on cherche en quel cas il est strictement négatif (afin
d'obtenir des racines complexes).

\begin{align*}
  \Delta &= (d-a)^2+4bc \\
         &= a^2-2ad+d^2+4bc \\
         &= a^2+2ad+d^2-4ad+4bc \\
         &= (a+d)^2-4(ad-bc) \\
         &= (a+d)^2-4.
\end{align*}

On voit donc que pour que $\tau$ soit effectivement fixé par $\gamma$,
ce dernier doit vérifier $\abs{a+d}<2$. On calcule aussi le polynôme
caractéristique de $\gamma$ qui va nous fournir les informations qui
nous manquent.
\[\chi_\gamma=X^2-(a+d)X+(ad-bc)=X^2-(a+d)X+1\]
Les valeurs de $\abs{a+d}$ limitent ce polynôme à trois cas : $X^2+1,
X^2-X+1$ et $X^2+X+1$. D'après le théorème de Cailey-Hamilton, on en
déduit $\gamma^4=I, \gamma^3=I$ ou $\gamma^6=I$ selon les cas, et donc
$\gamma$ est d'ordre $1,2,3,4$ ou $6$.

Le cas de l'ordre $1$ n'a que peu d'intérêt pour nous puisqu'il n'est
vérifié que si $\gamma=I$. Si $\gamma$ est d'ordre $2$, alors sachant
que $ad-bc=1$, on voit aisément que $\gamma=\pm I$, mais $I$ est
d'ordre $1$. Toutefois, ce cas n'est encore une fois pas d'un grand
intérêt ici car l'action de $-I$ est identique à celle de $I$. Nous
allons étudier les trois autres cas.

\begin{proposition}
  Soit $\gamma\in\SL$.
  \begin{enumerate}
  \item Si $\gamma$ est d'ordre $4$, alors $\gamma$ est conjugué à $
    \begin{pmatrix*}
      0 & -1 \\ 1 & 0
    \end{pmatrix*}^{\pm 1}$ dans $\SL$.
  \item Si $\gamma$ est d'ordre $6$, alors $\gamma$ est conjugué à $
    \begin{pmatrix*}
      0 & -1 \\ 1 & 1
    \end{pmatrix*}^{\pm 1}$ dans $\SL$.
  \item Si $\gamma$ est d'ordre $3$, alors $\gamma$ est conjugué à $
    \begin{pmatrix*}[r]
      0 & 1 \\ - 1 & -1
    \end{pmatrix*}^{\pm 1}$ dans $\SL$.
  \end{enumerate}
\end{proposition}

\begin{proof}
  ~\\[-0.5cm]
  \begin{enumerate}
  \item Suppsons $\gamma^4=I$. On peut alors munir le réseau $L=\Z^2$
    d'une structure de $\Z[i]$-module en définissant la loi externe
    suivante :
    \[\forall a,b\in\Z,\forall v\in L, (a+ib)\cdot v :=
    (aI+b\gamma)v.\]
    L'anneau $\Z[i]$ étant principal, et $L$ étant de type fini sur
    cet anneau, on peut utiliser le théorème de structure des modules
    de type fini sur un anneau principal.
    \[L\simeq \bigoplus_k \Z[i]/I_k\]
    où les différents $I_k$ sont des idéaux de $\Z[i]$. $L$ et tout
    idéal non nul de $\Z[i]$ sont libres de rang $2$ comme groupes
    abéliens, et par conséquent aucun quotient de $\Z[i]$ par un idéal
    non nul n'apparaît dans cette somme. Donc
    \[L\simeq \bigoplus_k \Z[i],\]
    et pour respecter les rangs, il n'y a en fait qu'un terme dans la
    somme.
    Il existe donc un isomorphisme de $\Z[i]$-modules
    $\phi_\gamma:\Z[i]\to L$.
    On pose $u=\phi_\gamma(1)$ et $v=\phi_\gamma(i)$. La famille
    $(u,v)$ est une base de $L$ et son déterminant est un inversible
    de $\Z[i]$, c'est-à-dire $1,-1,i$ ou $-i$. Or $u$ et $v$ sont à
    coefficients entiers, donc $\det(u,v)\in\{\pm 1\}$ (dans la base
    canonique).
    \[\gamma
    \begin{pmatrix}
      u&v
    \end{pmatrix}=
    \begin{pmatrix}
      v & -u
    \end{pmatrix}=
    \begin{pmatrix}
      u&v
    \end{pmatrix}
    \begin{pmatrix*}[r]
      0 & 1 \\ -1 & 0
    \end{pmatrix*}, \text{ d'où } \gamma=
    \begin{pmatrix}
      u&v
    \end{pmatrix}
    \begin{pmatrix*}[r]
      0 & 1 \\ -1 & 0
    \end{pmatrix*}
    \begin{pmatrix}
      u&v
    \end{pmatrix}^{-1},\]
    et
    \[\gamma
    \begin{pmatrix}
      v&u
    \end{pmatrix}=
    \begin{pmatrix}
      -u & v
    \end{pmatrix}=
    \begin{pmatrix}
      v&u
    \end{pmatrix}
    \begin{pmatrix*}[r]
      0 & -1 \\ 1 & 0
    \end{pmatrix*}, \text{ d'où } \gamma=
    \begin{pmatrix}
      v&u
    \end{pmatrix}
    \begin{pmatrix*}[r]
      0 & 1 \\ -1 & 0
    \end{pmatrix*}^{-1}
    \begin{pmatrix}
      v&u
    \end{pmatrix}^{-1}.\]
    Et comme $
    \begin{pmatrix}
      u&v
    \end{pmatrix}$ ou
    $\begin{pmatrix}
      v&u
    \end{pmatrix}$ est dans $\SL$, on a prouvé le premier point.
  \item Si $\gamma$ est d'ordre $6$, on procède de la même façon en
    remplaçant le $\Z[i]$-module par un $\Z[\mu_6]$-module (qui est
    encore un anneau principal) et l'on en déduit le résultat de la
    même manière.
  \item Si $\gamma$ est d'ordre $3$, alors $-\gamma$ est d'ordre $6$
    et le cas précédent permet aisément d'en déduire le résultat.
  \end{enumerate}
\end{proof}

\begin{corollaire}
  Les points elliptiques pour $\SL$ sont $\SL i$ et $\SL\mu_3$. La
  courbe modulaire $Y(1)$ a deux points elliptiques. Les groupes
  d'isotropie de $i$ et $\mu_3$ sont
  \[\SL_i = \left\langle
  \begin{pmatrix*}[r]
    0 & -1 \\ 1 & 0
  \end{pmatrix*}\right\rangle \text{ et } \SL_{\mu_3}=\left\langle
  \begin{pmatrix*}[r]
    0 & -1 \\ 1 & 1
  \end{pmatrix*}\right\rangle.\]
  Le sous-groupe d'isotropie $\SL_\tau$ de tout point elliptique
  $\tau$ de $\SL$ est cyclique.
\end{corollaire}


\begin{corollaire}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$. La courbe
  modulaire $Y(\Gamma)$ a un nombre fini de points elliptiques, et
  pour tout point elliptique $\tau$ de $\Gamma$, le sous-groupe
  d'isotropie $\Gamma_\tau$ est cyclique.
\end{corollaire}

\section{Pointes}

%\input{corps/geometrie-annexe}

Le domaine fondamental $\D$ se transfère à la sphère de Riemann en un
triangle ayant un sommet manquant (ce sommet étant le pôle nord de la
sphère). Il paraît naturel de compactifier ce triangle en ajoutant le
point manquant. La section présente cherche à généraliser cela pour
n'importe quelle courbe modulaire.

Le groupe $\GL$ agit sur $\Q\cup\{\infty\}$ de manière très similaire
à l'action de $\SL$ sur $\H$ par la règle :
\[\abcd\left(\frac{m}{n}\right)=\frac{am+bn}{cm+dn},\]
avec les conventions usuelles pour les manipulations de $\infty$ (le
cas $0/0$ ne se présentera pas). La restriction de cette action au
groupe $\SL$ est transitive.

Notons $\H^*=\H\cup\Q\cup\{\infty\}$. Nous allons munir cet espace
d'une topologie qui soit compatible avec celle de $\H$\footnote{Dans
  le sens où la restriction de cette nouvelle topologie à $\H$ nous
  donne celle que nous avions déjà en tant que sous-espace de
  $\CC$.}. Commençons, pour tout $M>0$, par définir les voisinages
\[\mathcal{N}_M=\{\tau\in\H:\Im{\tau}>M\}.\]
On prend alors comme topologie sur $\H^*$ celle qui est engendré par
la réunion de celle de $\H$ avec l'ensemble des parties
\[\alpha(\mathcal{N}_M\cup\{\infty\})\]
où $M>0$ et $\alpha\in\SL$, qui serviront de base de voisinages pour les pointes.

\begin{definition}
  Soit $\Gamma$ un sous-groupe de congruence de $\SL$. On définit le
  \textit{compactifié} de la courbe $Y(\Gamma)=\Gamma\setminus\H$ comme
  \[X(\Gamma)=\Gamma\setminus\H^*=Y(\Gamma)\cup\Gamma\setminus(\Q\cup\{\infty\}),\]
  où $\H^*= \H\cup\Q\cup\{\infty\}$ est muni de la topologie que nous
  venons de définir et $X(\Gamma)$ est muni de la topologie quotient.
\end{definition}

On rappelle qu'on appelle pointes les classes d'équivalence de
$\Q\cup\{\infty\}$ pour $\Gamma$. On appellera aussi pointes les points
$\Gamma s$ de $\Gamma\setminus(\Q\cup\{\infty\})$.

\begin{lemme}
  La courbe modulaire $X(1)=\SL\setminus\H^*$ n'a qu'une pointe. Pour
  tout sous-groupe de congruence $\Gamma$ de $\SL$, la courbe
  modulaire n'a qu'un nombre fini de pointes.
\end{lemme}

\begin{theoreme}
  Pour tout sous-groupe de congruence $\Gamma$ de $\SL$, la courbe modulaire $X(\Gamma)$ est séparée (au sens
  de Hausdorff), connexe et compacte.
\end{theoreme}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../mémoire"
%%% End:
