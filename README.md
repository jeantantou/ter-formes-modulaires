# TER — Formes modulaires

Mémoire de travail de recherche de M1 encadré par Benoît Stroh (IMJ-PRG), sur le sujet des formes modulaires. La principale ressource bibliographique était le livre A First Course in Modular Forms de Fred Diamond et Jerry Shurman.